({
	scriptsLoaded : function(component, event, helper) {
        var test_image = window.location.protocol +'//'+ window.location.host + $A.get('$Resource.ImageLib') + '/jQuery-Plugin-for-Image-Zoom-Rotation-Plugin-iviewer/test/test_image.jpg';
		var $ = jQuery;
        $(document).ready(function(){
            var iv1 = $("#viewer").iviewer({
                src: test_image
            });
        });
	},
    
    onChangeDocument : function(component, event, helper){
        var dokumen = event.getSource().get("v.value");
    	$("#viewer").iviewer('loadImage', dokumen);
	}
})